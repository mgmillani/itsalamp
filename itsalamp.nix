{ bash
, cmake
, gawk
, gdk-pixbuf
, glib
, gtk2
, lib
, libnotify
, pkg-config
, stdenv
, sysstat
, memon-settings ? 
  { low-threshold = 400
  ; high-threshold = 2000
  ; low-color     = "33DE00"
  ; high-color    = "FF7722"
  ; icons = []
  ;
  }
, cpumon-settings ? 
  { single-core-color = "55aaFF"
  ; all-cores-color = "FF3377"
  ; icons = []
  ;
  }
, monnet-settings ? 
  { download-high = 500 * 1024
  ; upload-high = 60 * 1024
  ; icon = ""
  ;
  }
, itsalarm-settings ?
  { icons = []
  ;
  }
, ...
} :

let memon-settings-final = ''
LOW=${builtins.toString memon-settings.low-threshold}
HIGH=${builtins.toString memon-settings.high-threshold}
LOWC=${memon-settings.low-color}
HIGHC=${memon-settings.high-color}
'' +
(if memon-settings.icons == [] then
''
ICONS=('$out/share/icons/itsalamp/trizza-t0.svg' '$out/share/icons/itsalamp/trizza-t1.svg' '$out/share/icons/itsalamp/trizza-t2.svg')
''
      else
''
ICONS=('${lib.concatStringsSep "' '" memon-settings.icons}')
'');
    cpumon-settings-final = ''
SINGLECOLOR=${cpumon-settings.single-core-color}
FULLCOLOR=${cpumon-settings.all-cores-color}
'' + 
(if cpumon-settings.icons == [] then
''
ICONS=('$out/share/icons/itsalamp/speed arrow-t0.svg' '$out/share/icons/itsalamp/speed arrow-t1.svg' '$out/share/icons/itsalamp/speed arrow-t2.svg')
''
else
''
ICONS=('${lib.concatStringsSep "' '" cpumon-settings.icons}')
'');
    monnet-settings-final = ''
DHIGH=${builtins.toString monnet-settings.download-high}
UHIGH=${builtins.toString monnet-settings.upload-high}
'' + 
(if monnet-settings.icon == "" then
''
ICON='$out/share/icons/itsalamp/double-circles.svg'
''
else
''
ICON='${monnet-settings.icon}'
'');
    itsalarm-settings-final = if itsalarm-settings.icons == [] then
    ''DEFAULT_ICONS=('$out/share/icons/itsalamp/wallclock-t0.svg' '$out/share/icons/itsalamp/wallclock-t1.svg' '$out/share/icons/itsalamp/wallclock-t2.svg' '$out/share/icons/itsalamp/wallclock-t3.svg' '$out/share/icons/itsalamp/wallclock-t4.svg')
''
  else
''DEFAULT_ICONS=('${lib.concatStringsSep "' '" itsalarm-settings.icons}')''
;
in
stdenv.mkDerivation {
  name = "itsalamp";
	src = ./.;
  # nativeBuildInputs = [ cmake pkg-config ];
  buildInputs = [ cmake gtk2 gdk-pixbuf glib pkg-config];
	cmakeFlags = 
  [ "-DCMAKE_BUILD_TYPE=Release"
    "-DGTK2_GLIBCONFIG_INCLUDE_DIR=${glib.out}/lib/glib-2.0/include"
    "-DGTK2_GDKCONFIG_INCLUDE_DIR=${gtk2.out}/lib/gtk-2.0/include"
  ];
  
  postInstall = ''
mkdir -p $out/bin
cat << EOF > $out/bin/memon-tmp
#!${bash}/bin/bash
${memon-settings-final}
EOF
tail -n+8 $out/bin/memon >> $out/bin/memon-tmp
mv $out/bin/memon-tmp $out/bin/memon
chmod +x $out/bin/memon

cat << EOF > $out/bin/cpumon-tmp
#!${bash}/bin/bash
${cpumon-settings-final}
EOF
tail -n+5 $out/bin/cpumon >> $out/bin/cpumon-tmp
mv $out/bin/cpumon-tmp $out/bin/cpumon
chmod +x $out/bin/cpumon

cat << EOF > $out/bin/monnet-tmp
#!${bash}/bin/bash
${monnet-settings-final}
EOF
tail -n+5 $out/bin/monnet >> $out/bin/monnet-tmp
mv $out/bin/monnet-tmp $out/bin/monnet
chmod +x $out/bin/monnet

cat << EOF > $out/bin/itsalarm-tmp
#!${bash}/bin/bash
PATH=\$PATH:${libnotify}/bin
${itsalarm-settings-final}
EOF
tail -n+4 $out/bin/itsalarm >> $out/bin/itsalarm-tmp
mv $out/bin/itsalarm-tmp $out/bin/itsalarm
chmod +x $out/bin/itsalarm
	'';

}
