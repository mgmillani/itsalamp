{ stdenv
, bash
, buildPackages
, callPackage
, cmake
, gawk
, gdk-pixbuf
, glib
, gtk2
, lib
, librsvg
, makeWrapper
, runCommand
, sysstat
, gdk-extra-modules ? []
, ...
} :

let itsalamp = callPackage ./itsalamp.nix { };
    effectivePackages = lib.unique ([gdk-pixbuf librsvg] ++ gdk-extra-modules);
    gdk-loader-cache = runCommand "gdk-pixbuf-loaders.cache" { preferLocalBuild = true; } ''
      (
        for package in ${lib.concatStringsSep " " effectivePackages}; do
          module_dir="$package/${gdk-pixbuf.moduleDir}"
          if [[ ! -d $module_dir ]]; then
            echo "Warning (itsalamp): missing module directory $module_dir" 1>&2
            continue
          fi
          GDK_PIXBUF_MODULEDIR="$module_dir" \
            ${stdenv.hostPlatform.emulator buildPackages} ${gdk-pixbuf.dev}/bin/gdk-pixbuf-query-loaders
        done
      ) > "$out"
    '';
in
stdenv.mkDerivation {
  name = "itsalamp-wrapped";
  # src = ./include;
	buildInputs = [ makeWrapper ];
  dontUnpack = true;

  configurePhase = "";
  buildPhase = "echo 0";

	installPhase = ''
makeWrapper ${itsalamp}/bin/itsalamp $out/bin/itsalamp \
  --set GDK_PIXBUF_MODULE_FILE ${gdk-loader-cache}

makeWrapper ${itsalamp}/bin/cpumon $out/bin/cpumon --prefix PATH : ${lib.makeBinPath [gawk sysstat]}

ln -s ${itsalamp}/bin/itsacounter $out/bin/itsacounter
ln -s ${itsalamp}/bin/monnet $out/bin/monnet
ln -s ${itsalamp}/bin/memon  $out/bin/memon
ln -s ${itsalamp}/bin/itsalarm  $out/bin/itsalarm
'';
}
